var template = $('#template').val().replace(/\\r\\n/g, '').replace(/\\/g,"");
var operate = 'add';

var goodsName = $('.name');
var goodsPrice = $('.price');
var goodsDec = $('.dec');
var goodsCount = $('.count');
var goodsImg = $(".img");
var goodsDiscount = $(".discount");
var goodsUnite = $(".unite");
var goodsId = null;

function render() {
    $.ajax({
        type:"get",
        url:"/api/all",
        async:true,
        success:function(Ret){
            if(Ret.iRet == 0){
                var html = ejs.render(template,{data:Ret.data});
                $("table").html(html);
            }else{
                console.log("获取全部数据错误!",Ret);
            }

        },
        error:function(error){
            console.log(error);
        }
    });
}

var commandList = {
    add:function () {
        commandList.url =  '/api/add';
        commandList.getDate();

    },
    update:function (that) {
        commandList.url = '/api/update';
        commandList.getDate();
    },
    del:function () {
        commandList.url = '/api/delete';
        commandList.getDate();
    },
    getDate:function (data) {
        $.ajax({
            url:  commandList.url,
            data: {
                goodsName: goodsName.val(),
                goodsPrice: goodsPrice.val(),
                goodsDec: goodsDec.val(),
                goodsCount: goodsCount.val(),
                goodsId:goodsId,
                goodsDiscount:goodsDiscount.val(),
                goodsImg:goodsImg.val(),
                goodsUnite:goodsUnite.val()
            },
            method: 'get'
        }).done(function (data) {
            console.log(data);
            if(data.iRet == 0){
                render();
            }
            if (data.iRet == -11){
                alert("数据重复");
            }
        })
            .fail(function (error) {
                console.error(error);
            });
    }
}


//取消
$('.cover-wrap').on("click",function (e) {
    if($(e.target).hasClass('cover-wrap')){
        $(this).addClass('hide');
        clear();

    }
});
//取消
$('.cover-wrap').on("click",'.cancel',function (e) {
    $('.cover-wrap').addClass('hide');
    clear();
});
//确认
$('.cover-wrap').on("click",'.enter',function (e) {
    if( goodsName.val() == '' ||goodsPrice.val()== '' ||  goodsDec.val() == '' || goodsCount.val() == ''){
        alert('完善数据');
    }else {
        commandList[operate]();
        $('.cover-wrap').addClass('hide');
        clear();
    }
});

//增加
$('button.add').on('click',function () {
    operate = 'add';
    $('.cover-wrap').removeClass('hide');
});

//修改
$('table').on('click','span.update',function () {
    operate = 'update';
    var $tr = $(this).parents('tr');

    goodsName.val($tr.find('td.td-name').html());
    goodsPrice.val($tr.find('td.td-price').html());
    goodsDec.val($tr.find('td.td-dec').html());
    goodsCount.val($tr.find('td.td-count').html());
    goodsCount.val($tr.find('td.td-count').html());
    goodsDiscount.val($tr.find('td.td-discount').html());
    goodsImg.val($tr.find('td.td-img').find("img").attr('src'));
    goodsUnite.val($tr.find('td.td-unite').html())
    goodsId = $tr.find('td.td-id').html();

    $('.cover-wrap').removeClass('hide');
});
//删除
$('table').on('click','span.del',function () {
    var $tr = $(this).parents('tr');
    goodsId = $tr.find('td.td-id').html();
    commandList['del']();
});

function  clear() {
    goodsName.val('');
    goodsPrice.val('');
    goodsDec.val('');
    goodsCount.val('');
    goodsDiscount.val("");
    goodsImg.val("");
    goodsUnite.val("");
}


