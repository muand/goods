var co = require('co');
var Q = require('q');


function  p1() {
    var deferred = Q.defer();

    setTimeout(function () {
        deferred.resolve({"data":1});
    },1000);
    return deferred.promise;
}


function  p2() {
    var deferred = Q.defer();
    setTimeout(function () {
        deferred.resolve({"data":2});
    },2000);
    return deferred.promise;
}

co(function *() {
    var res2 = yield  p2();
    console.log(res2);
    var res1 = yield  p1();
    console.log(res1);
})