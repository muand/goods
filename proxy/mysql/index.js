//连接数据库
var mysql = require('mysql');
var sqlConfig = require('../../lib/mysql');
var mySqlParams = {
    connectionLimit : sqlConfig.connectionLimit,
    host: sqlConfig.host, //数据库地址
    user: sqlConfig.user,   //数据库用户名
    password: sqlConfig.password, //数据库管理密码
    database:sqlConfig.database, //数据库名称
    insecureAuth: sqlConfig.insecureAuth
}

var pool  = mysql.createPool(mySqlParams);

module.exports = function (operate) {
    var prom  = new Promise(function (resolve, reject) {

        pool.query(operate, function(err, rows, fields) {
            try {
                if (err) {
                    reject({
                        error:err
                    });
                    throw err;
                };
                resolve(rows);
            }catch (error){
                console.log(error);
            }

        });

    });
    return prom;
}

