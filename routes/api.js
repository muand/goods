var express = require('express');
var router = express.Router();
var dbOperate = require('../lib/dbOperate');

router.get('/all', getAllData, function (req, res, next) {
    res.send(res.locals.data);
});

router.get('/add',addData, function (req, res, next) {
    res.send(res.locals.data);
});

router.get('/delete', delet, function (req, res, next) {
    res.send(res.locals.data);
});

router.get('/update',upData, function (req, res, next) {
    res.send(res.locals.data);
});

//获取所有的数据
function getAllData(req, res, next) {
    if (!req.query) {
        req.query = {};
    }
    dbOperate.sql({
        sql:'select * from goods order by id ASC'
    }).then(function (Ret) {
        res.locals.data = Ret;
        next();
    },function (Ret) {
        res.locals.data = [];
        next();
    }).done();
}



//增加数据
function addData(req, res, next) {
    if (!req.query) {
        req.query = {};
    }
    var data = dataFormateToDB(req.query);
    var option = {
        tableName:'goods',
        data:data,
        reuse:{
            name:data.name,
            descript:data.descript
        }
    }
    dbOperate.insert(option).then(function (insertRet) {
        res.locals.data = insertRet;
        next();
    },function (insertRet) {
        res.locals.data = insertRet;
        next();
    }).done();
}

//修改
function upData(req, res, next) {
    if (!req.query) {
        req.query = {};
    }
    var goodsId = req.query.goodsId;
   var data = dataFormateToDB(req.query);
    var option = {
        data:data,
        tableName:'goods',
        where:{
            id:goodsId
        }
    }
    dbOperate.update(option).then(function (Ret) {
        res.locals.data = Ret;
        next();
    },function (Ret) {
        res.locals.data = Ret;
        next();
    }).done();

}

//删除
function delet(req, res, next) {
    if (!req.query) {
        req.query = {};
    }
    var goodsId = req.query.goodsId;
    var option ={
        tableName:'goods',
        where:{
            id:goodsId
        }
    }
    dbOperate.delete(option).then(function (Ret) {
        res.locals.data = Ret;
        next();
    },function (Ret) {
        res.locals.data = Ret;
        next();
    }).done();

}


function dataFormateToDB(data) {
    var goodsName = decodeURI( data.goodsName);
    var goodsPrice =  decodeURI(  data.goodsPrice);
    var goodsDec = decodeURI(  data.goodsDec);
    var goodsCount =  data.goodsCount;
    var goodsImg = decodeURI( data.goodsImg) || "https://img.alicdn.com/bao/uploaded/i1/TB1Mb3YPpXXXXXUXpXXXXXXXXXX_!!0-item_pic.jpg_430x430q90.jpg";
    var goodsDiscount = data.goodsDiscount || -1;
    var goodsUnite = data.goodsUnite;
    var obj = {
        name:goodsName,
        price:goodsPrice,
        descript:goodsDec,
        count:goodsCount,
        img:goodsImg,
        discount:goodsDiscount,
        unite:goodsUnite
    }
    return obj;
}

module.exports = router;
