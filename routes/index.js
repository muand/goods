var express = require('express');
var router = express.Router();

var fs = require('fs');
var dbOperate = require('../lib/dbOperate');
/* GET home page. */
router.get('/', getTemplate,getAllData,function(req, res, next) {
  res.render('index');
});

router.get('/api', getTemplate,getAllData,function(req, res, next) {
  var p  = req.query.p || '';
  if(p == 1){
    res.status(404).send('404 Not found...');
    return ;
  }else{
    res.send({success:1,orgin:req.originalUrl,host:req.headers,req:req.baseUrl});
  }

  console.log("===============")

});
//获取模板
function  getTemplate(req,res,next) {
  res.locals.template = fs.readFileSync('../views/list.ejs',{encoding:'utf-8'});
  next();
}



function getAllData(req,res,next) {
  dbOperate.sql({
    sql:'select * from goods order by id ASC '
  }).then(function (Ret) {
    res.locals.data = Ret.data;
    next();
  },function (Ret) {
    res.locals.data = [];
    next();
  }).done();

}


module.exports = router;
