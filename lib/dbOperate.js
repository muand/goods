var mysql = require('../proxy').mysql;
var dbOperate = {};
var Q = require('q');

/**
 *option{
 *      sql:''
 * }
 *
 * return {
 *      iRet,
 *      message,
 *      data
 * }
 * */
dbOperate.sql = function (option) {
    var deferred = Q.defer();
    var option = option || {};
    var sql  = option["sql"] ||"";
    var rsp = {
        iRet:0,
        message:'success',
        data:[]
    }
    if(!sql){
        process.nextTick(function(){
            rsp.iRet = -4;
            rsp.message = "update参数非法";
            deferred.resolve(rsp);
        });
    }

    mysql(sql).then(function (data) {
        rsp.data = data;
        deferred.resolve(rsp);
    }).catch(function (error) {
        rsp.iRet = -10;
        rsp.message = "操作数据库错误,sql:"+sql;
        deferred.resolve(rsp);
    });
    return deferred.promise;
}


/**
 * option{
 *      tableName:(string)
 *      data{},
 *      where:{}
 * }
 *
 * return {
 *      iRet(int)
  *      message(String)
 * }
* */
dbOperate.update  = function (option) {
    var deferred = Q.defer();
    var option = option || {};
    var data  = option["data"] ||{};
    var where = option["where"] || {};
    var tableName = option["tableName"] || '';
    var judge = option['judge'] || 'and';
    var rsp = {
        iRet:0,
        message:'success'
    }
    if(JSON.stringify(data) == '{}' || !tableName){
        process.nextTick(function(){
            rsp.iRet = -4;
            rsp.message = "update参数非法";
            deferred.resolve(rsp);
        });
    }

    var dataArray = [];
     for(var i in data){
         var str = i + '="'+data[i]+'"';
         dataArray.push(str);
     }
     var whereArray = [];
     for(var i in where){
         if(whereArray.length == 0){
            var str =  i + '="'+where[i] + '" ';
         }else{
             var str = judge + ' ' +  i + '="'+where[i] + '" ';
         }
         whereArray.push(str);
     }
    if(whereArray.length == 0){
        var sql  =  'UPDATE '+ tableName +' SET '+ dataArray.join(",") ;
    }else{
        var sql  =  'UPDATE '+ tableName +' SET '+ dataArray.join(",") + ' where '+ whereArray.join(' ');
    }


    dbOperate.sql({sql:sql}).then(function (sqlRet) {
            if(sqlRet.iRet == 0){
                deferred.resolve(rsp);
            }else{
                rsp.iRet = -10;
                rsp.message = "操作数据库错误,sql:"+sql;
                deferred.resolve(rsp);
            }

        },function (sqlRet) {
        rsp.iRet = -10;
        rsp.message = "操作数据库错误,sql:"+sql;
        deferred.resolve(rsp);
     }).done();


    return deferred.promise;
}




/**
 * insert数据
 * option{
 *      data:'',
 *      tableName:''
 * }
* */
dbOperate.insert = function (option) {
    var deferred = Q.defer();
    var option = option || {};
    var data  = option["data"] ||{};
    var tableName = option['tableName'] || '';
    var reuse = option['reuse'] || {};
    var judge = option['judge'] || 'and';
    var rsp = {
        iRet:0,
        message:'success'
    }
    if(!tableName || JSON.stringify(data) == '{}'){
        process.nextTick(function(){
            rsp.iRet = -4;
            rsp.message = "update参数非法";
            deferred.resolve(rsp);
        });
    }
    var reuseArray = [];
    for(var i in reuse){
        if(reuseArray.length == 0){
            var str =  i + '="'+reuse[i] + '" ';
        }else{
            var str = judge + ' ' +  i + '="'+reuse[i] + '" ';
        }
        reuseArray.push(str);
    }
    if(reuseArray.length == 0){
        var getDataSql = 'select id from '+ tableName ;
    }else{
        var getDataSql = 'select id from '+ tableName + ' where '+ reuseArray.join(' ');
    }

    var dataArray = [];
    var keyArray = [];
    for(var i in data){
        var str = '"'+data[i] + '"';
        keyArray.push(i);
        dataArray.push(str);
    }
    dbOperate.sql({sql:getDataSql}).then(function (getDataRet) {
       if(getDataRet.iRet == 0){
           if(getDataRet.data.length == 0){
                var insertSql = 'INSERT INTO '+ tableName + ' (' + keyArray .join(',')+ ' ) VALUES(  ' + dataArray.join(',')   + ')';
               dbOperate.sql({sql:insertSql}).then(function (insertSqlRet) {
                    if(insertSqlRet.iRet == 0){
                        deferred.resolve(rsp);
                    }else{
                        rsp.iRet = -11;
                        rsp.message = "操作数据库错误0,sql:"+insertSql;
                        deferred.resolve(rsp);
                    }
               },function (insertSqlRet) {
                   rsp.iRet = -11;
                   rsp.message = "操作数据库错误0,sql:"+insertSql;
                   deferred.resolve(rsp);
               }).done();
           } else{
               rsp.iRet = -11;
               rsp.message = "插入的数据重复,sql:"+getDataSql;
               deferred.resolve(rsp);
           }
       }else{
           rsp.iRet = -10;
           rsp.message = "操作数据库错误1,sql:"+getDataSql;
           deferred.resolve(rsp);
       }
    },function (getDataRet) {
        rsp.iRet = -10;
        rsp.message = "操作数据库错误2,sql:"+getDataSql;
        deferred.resolve(rsp);
    }).done();


    return deferred.promise;
}

/**
 * 获取数据
 * option{
 *    list:[],  需要获取的字段，不写表示获取所有
 *    where:
 *    judge:  是 and还是or,默认位or
 *    tableName:
 * }
 * return {
 *      iRet:,
 *      message:,
 *      data:[]
 * }
* */
dbOperate.getData = function (option) {
    var deferred = Q.defer();
    var option = option || {};
    var list  = option["list"] ||[];
    var tableName = option['tableName'] || '';
    var where = option["where"] || {};
    var judge = option['judge'] || 'and';
    var rsp = {
        iRet:0,
        message:'success',
        data:[]
    }
    if( !tableName){
        process.nextTick(function(){
            rsp.iRet = -4;
            rsp.message = "update参数非法";
            deferred.resolve(rsp);
        });
    }

    var whereArray = [];
    for(var i in where){
        if(whereArray.length == 0){
            var str =  i + '="'+where[i] + '" ';
        }else{
            var str = judge + ' ' +  i + '="'+where[i] + '" ';
        }
        whereArray.push(str);
    }
    var keyItem = '';
    if(list.length == 0){
        keyItem = '*'
    }else{
        keyItem = list.join(',')
    }

    if(whereArray.length == 0){
        var sql  =  'select '+ keyItem +' from ' + tableName;
    }else{
        var sql  = 'select '+ keyItem +' from ' + tableName +' where '+ whereArray.join(" ") ;
    }

    dbOperate.sql({sql:sql}).then(function (sqlRet) {
        if(sqlRet.iRet == 0){
            rsp.data = sqlRet.data;
            deferred.resolve(rsp);
        }else{
            rsp.iRet = -10;
            rsp.message = "操作数据库错误,sql:"+sql;
            deferred.resolve(rsp);
        }
    },function (sqlRet) {
        rsp.iRet = -10;
        rsp.message = "操作数据库错误,sql:"+sql;
        deferred.resolve(rsp);
    }).done();

    return deferred.promise;
}

/**
 * 删除
 *
* */
dbOperate.delete = function (option) {
    var deferred = Q.defer();
    var option = option || {};
    var tableName = option['tableName'] || '';
    var where = option["where"] || {};
    var judge = option['judge'] || 'and';
    var rsp = {
        iRet:0,
        message:'success'
    }
    if( !tableName){
        process.nextTick(function(){
            rsp.iRet = -4;
            rsp.message = "update参数非法";
            deferred.resolve(rsp);
        });
    }

    var whereArray = [];
    for(var i in where){
        if(whereArray.length == 0){
            var str =  i + '="'+where[i] + '" ';
        }else{
            var str = judge + ' ' +  i + '="'+where[i] + '" ';
        }
        whereArray.push(str);
    }
    if(whereArray.length == 0){
        var sql  =  'DELETE  from ' + tableName;
    }else{
        var sql  = 'DELETE  from ' + tableName +' where '+ whereArray.join(" ") ;
    }
    dbOperate.sql({sql:sql}).then(function (sqlRet) {
        if(sqlRet.iRet == 0){
            deferred.resolve(rsp);
        }else{
            rsp.iRet = -10;
            rsp.message = "操作数据库错误,sql:"+sql;
            deferred.resolve(rsp);
        }
    },function (sqlRet) {
        rsp.iRet = -10;
        rsp.message = "操作数据库错误,sql:"+sql;
        deferred.resolve(rsp);
    }).done();

    return deferred.promise;
};


module.exports = dbOperate;
